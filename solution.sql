-- insert into users
insert into blog_db.users(email,password,datetime_created)
VALUES ("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00"),
("juandelacruz@gmail.com","passwordB","2021-01-01 02:00:00"),
("janesmith@gmail.com","passwordC","2021-01-01 03:00:00"),
("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00"),
("johndoe@gmail.com","passwordE","2021-01-01 05:00:00");

-- insert into posts
insert into blog_db.posts(author_id,title,content,datetime_posted)
VALUES (1,"First Code","Hello World!","2021-01-02 01:00:00"),
(1,"Second Code","Hello Earth!","2021-01-02 02:00:00"),
(2,"Third Code","Welcome to Mars!","2021-01-02 03:00:00"),
(4,"Fourth Code","Bye bye solar system!","2021-01-02 04:00:00");

-- all posts where author id=1
SELECT * from blog_db.posts WHERE author_id=1;

-- get all user's email and datetime of creation;
SELECT email, datetime_Created from users;

-- update a posts content to hello to the people of the earth where content is hello earth by using record IDENTIFIED
UPDATE posts set content="Hello to the people of the Earth" where id=2;

-- delete user with an email of johndoe@gmail.com; used wildcard just in case mali yung spacing ko

DELETE from users where email LIKE "%johndoe@gmail%";